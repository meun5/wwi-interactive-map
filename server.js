import express from 'express';
import path from 'path';
import nl2br from 'nl2br';
import fs from 'fs';
import config from './config.json';

const app = express();

app.use("/static", express.static("dist"));

app.get("/", (request, response) => {
   response.sendFile(path.join(__dirname, "/index.html"));
});

app.get("/wwi-map", (request, response) => {
    response.sendFile(path.join(__dirname, "/wwi-map/index.html"));
});

app.get("/wwi-map/license", (request, response) => {
    response.set('Content-Type', 'text/html');
    response.send(
        nl2br(fs.readFileSync(
            path.join(__dirname, "/wwi-map/LICENSE")
        ))
    );
});

app.get("/wwii-newspaper", (request, response) => {
    response.sendFile(path.join(__dirname, "/wwii-newspaper/index.html"));
});

app.listen(process.env.port || config.port, () => {
   console.log("Server listening on: " + (process.env.port || config.port)); 
});