import React , { Component } from 'react';
import { render as DOMRender } from 'react-dom';
import $ from 'jquery';
import _ from 'lodash';
import imageMapResize from 'image-map-resizer';
import coords from './coords.json';

export default class MapMaker extends Component {
	constructor() {
		super();

        this.state = {
            shownCountryId: 0,
            shownCountryInfo: {
                "title": "Please Select A Country",
                "info": {
					"pop": null,
					"pop_wartime": null,
					"cause_for_war": null,
					"alliance": null,
					"troops_lost_to_war": null,
					"leader_at_time": null,
				},
				"sources": {
					"pop": null,
					"pop_wartime": null,
					"cause_for_war": null,
					"alliance": null,
					"troops_lost_to_war": null,
					"leader_at_time": null,
				}	
            },
        };
		
		this.refs = [];

        this.showCountry = this.showCountry.bind(this);
        this.lookupContryInfoAndSources = this.lookupContryInfoAndSources.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);

        this.countrys = coords;
	}

	componentDidMount() {
		imageMapResize();
	}
	
    showCountry(id) {
        let info = this.lookupContryInfoAndSources(id);
		
        this.setState({
            shownCountryId: id,
            shownCountryInfo: info
        });
    }

    lookupContryInfoAndSources(id, getSources = true) {
        id = (id === null) ? this.state.shownCountryId : id;
		
        return this.countrys.find((element) => {
            return element.id == id;
        });
    }
	
	handleSelectChange(event) {
		this.showCountry(event.target.value);
	}
	
	render() {
		const doShowCountry = !(this.state.shownCountryId === 0);
		return (
			<div id="parent">
				<div className="row expanded">
					<div id="select" className="small-2 columns">
						<select value={this.state.shownCountryId} onChange={this.handleSelectChange}>
							{this.countrys.map((country) => {
                                return (
                                    <option key={country.id} value={country.id} title={country.title}>{country.title}</option>
                                );
                            })}
						</select>
						<span className="stick-bottom small-text">Country click areas are approximate. For best results, click in the center of the country or use the drop down menu above.</span>
					</div>
                    <div id="image-container" className="small-6 columns">
                        <img src="/static/map.gif" useMap="#image-map" width="715" height="474"></img>
                        <map name="image-map">
                            {this.countrys.map((country) => {
                                return (
                                    <area key={country.id} target="_top" alt={country.title} title={country.title} href="#" coords={country.coords} shape="poly" onClick={() => this.showCountry(country.id)} />
                                );
                            })}
                        </map>
                    </div>
                    <div id="country-info" className="small-3 columns">
                        <p>{this.state.shownCountryInfo.title}</p>
						<hr />
                        <p>Current Pop: {this.state.shownCountryInfo.info.pop}</p>
                        <p>Pre-Wartime Pop: {this.state.shownCountryInfo.info.pop_wartime}</p>
                        <p>Alliance: {this.state.shownCountryInfo.info.alliance}</p>
                        <p>Cause to Enter War: <br />{this.state.shownCountryInfo.info.cause_for_war}</p>
                        <p>Troops Lost to War: {this.state.shownCountryInfo.info.troops_lost_to_war}</p>
                        <p>Leader At War Time: {this.state.shownCountryInfo.info.leader_at_time}</p>
                    	<h6 className="small-text">* All Number are Approximate</h6>
					</div>
				</div>
				<div className="row expanded">
					<div key={Math.round(Math.random() * 112 * Math.random())} className="small-12 columns">
						<h6>Sources</h6>
						<hr />
					</div>
					{_.map(this.state.shownCountryInfo.sources, (value, key) => {
						let [text, target] = _.split(value, " | ");
						let [author, name] = _.split(text, ",");
						return (
							<div key={Math.round(Math.random() * 112 * Math.random())} className="small-2 columns">
								<h6 key={Math.round(Math.random() * 112 * Math.random())} className="small-text">{_.startCase(key)}:</h6>
								<h6 key={Math.round(Math.random() * 112 * Math.random())} className="small-text"><a href={target}>{author},<br />{name}</a></h6>
							</div>
						);
					})}
				</div>
				<div className="footer row expanded">
					<div className="small-12 columns">
						<span className="small-text" id="copyright">Copyright &copy; 2016 Alexander Young. All Rights Reserved. Licensed under the <a href="/license">MIT License</a></span>
					</div>
				</div>
			</div>
		);
	}
}

DOMRender(<MapMaker />, $("#react-stem").get(0));
