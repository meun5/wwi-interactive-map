import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Main extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <h1 className="header">Japan Attacks US Naval Base</h1>
                    <h4 className="reporter">Reported by Alexander Young</h4>
                </div>
                <div className="row paragraph">
                    <div id="paragraph-one" className="col-8">
                        <p>
                            In a preemptive strike to keep the US Pacific Fleet out of its way, 
                            the Imperial Japanese Army has launched a massive surprise attack on 
                            the US Naval and Air Base in Pearl Harbor, on December 7th, 1941.
                        </p>
                        <p>
                            In all, a total of 4 US battleships were sunk and another 4 damaged.
                            2,403 total brave service members paid the ultimate sacrifice in the attack, 
                            with another 1,178 severly wounded. 
                        </p>
                    </div>
                    <div id="image-one" className="col-4">
                        <img src="/static/wwii-newspaper/uss-arizona-burning.jpeg" className="rounded img-thumbnail" alt="USS Arizona Burning after an attack by Japanese Bombers" />
                    </div>
                </div>
                <div className="row paragraph">
                    <div id="image-two" className="col-4">
                        <img src="/static/wwii-newspaper/japan-invasion-plan.jpg" className="rounded img-thumbnail" alt="Pearl Harbor Attack Plan for Japanese Aircraft Carrier Task Force" />
                    </div>
                    <div id="paragraph-two" className="col-8">
                        <p>
                            It all began when, stunted by low natural resouces, Japan invaded a resource-rich area
                            of Northern China. The United States, in an attempt to prohibit Japanese expansion in the 
                            Pacfic and to help protect key British terroritories in the region, set up
                            a trade embargo, which banned the sale of strategic materials to Japan. The Japanese, angered by the 
                            trade embargo, planned and executed what was called Operation AI. Operation AI involved bombing the United States
                            Pacific Fleet, which happened to be stationed in Pearl Harbor in the Hawaii Terroritory.
                        </p>
                    </div>
                </div>
                <div className="row paragraph">
                    <div id="paragraph-three" className="col-8">
                        <p>
                            The Japanese attacked using aircraft carriers stationed a few miles off the coast of Hawaii. They used 408
                            aircraft, 360 in the first wave and 48 in the second wave, carring armour piercing bombs and torpedos. 
                            The Japanese also utilized Midget Submarines, which carried specially designed weapons,
                            to counter-attack oncoming destroyers and aid-ships. The first Japanese shots were fired at 0748 hours, Hawaiian time, when the first
                            wave of 358 aircraft bombed the harbor. Onlookers of the ordeal would have seen something like this,
                        </p>
                    </div>
                    <div id="image-three" className="col-4">
                        <img src="/static/wwii-newspaper/japan-carrier-task-force-plan.jpg" className="rounded img-thumbnail" alt="Pearl Harbor Attack Plan for Japanese Aircraft Carrier Task Force" />
                    </div>
                    <div className="col-12">
                        <p>
                            <em>"...Ships and planes everywhere, up in flames. The great mighty battleships, sunk and on fire. The aircraft hangers, burning and wasted. Such a great loss we have sustained today." -- Richard Adam, 1943</em>
                        </p>
                        <p>
                            Servicemen Tommy Schmit, recalls the event like this,<br/>
                            <em>
                                "...They were coming over the hills, heading straight for Battleship Row, 
                                [The Japanese] bombed every ship in sight...
                                The Ships start sinking with her men jumping overboard, away from the flames..." -- Tommy Schmit, 1941
                            </em>
                        </p>
                    </div>
                </div>
                <div className="row paragraph">
                    <div id="image-four" className="col-4">
                        <img src="/static/wwii-newspaper/infamy-speech.jpg" className="rounded img-thumbnail" alt="Infamy Speech to America" />
                    </div>
                    <div id="paragraph-four" className="col-8">
                        <p>
                            The lives of not only the survivers, but also the American public, was forever changed.
                            Public opinion of the war changed in a heartbeat. In fact the very next day, in a nearly unanimous vote,
                            congress declared war on Japan. President Franklin D. Roosevelt, delivered his famous Infamy Speech, which 
                            also played a great role in swaying public opinion. Pearl Harbor and Roosevelt's Infamy Speech are referenced, even
                            today, in propoganda, comics, and other writings. 
                        </p>
                        <p>
                            That wasn't the end for Pearl Harbor though. Since important strategic and repair facilities had been spared, Pearl Harbor
                            continued to be an important asset to the United States. All but three battleships were raised, repaired, and returned
                            to service in the war.
                        </p>
                    </div>
                    <div className="col-12">
                        <p>
                            There have been some critism for the Pearl Harbor attack from Japanese officals. Admiral Hara Tadaichi summed up the Japanese result by saying, 
                            "We won a great tactical victory at Pearl Harbor and thereby lost the war." A quote from Isoroku Yamamoto, the Admiral who originally planned the attack,
                            "I fear all we have done is to awaken a sleeping giant and fill him with a terrible resolve."
                        </p>
                    </div>
                </div>
                <div className="row offset-2 col-8" style={{borderTop: "1px solid #EFEFEF", textAlign: "center", marginTop: "1rem", marginBottom: "1rem"}}>
                    <small>‡ The names that appear in this document are real. The quotes, with the exception of the Japanese Officals, are not.</small><br/>
                    <small><em>Rendered with help of React, and Nodejs v7</em></small>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Main />, document.getElementById("react-root"));