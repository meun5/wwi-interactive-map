const express = require("express");
const path = require("path");
const nl2br = require("nl2br");
const fs = require("fs");

const app = express();

app.set('port', (process.env.PORT || 3000));

app.use("/static", express.static("dist"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get("/license", (req, res) => {
  res.set('Content-Type', 'text/html');
  const text = fs.readFileSync(path.join(__dirname + '/LICENSE'));
  res.send(nl2br(text));
});

app.listen(app.get('port'), () => {
  console.log("Example app listening on port " + app.get('port'));
});