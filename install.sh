mkdir -p dist/
mkdir -p logs/ > mkdir-log.log
npm install --dev > logs/dev.log
NODE_ENV=production npm run build > jsx.log
npm run build:css > logs/css.log
cp Map.gif dist/map.gif > logs/cp.log
exit 0